import { StorageService } from './../services/storage.service';
import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[ngPermission]'
})
export class NgPermissionDirective {

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private storageService: StorageService
    ) {}

    @Input()
    set ngPermission(val) {
        const accessToken = this.storageService.getItem(StorageService.ACCESS_TOKEN)
        const tokenInfo = JSON.parse(atob(accessToken.split('.')[1]));
        console.log(val);
        console.log(tokenInfo.authorities);
        if (tokenInfo.authorities.includes(val)) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainer.clear();
        }
    }
}