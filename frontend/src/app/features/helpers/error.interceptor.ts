import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { LoginService } from './../services/login.service';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private loginService: LoginService, private router: Router) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if([403].includes(err.status)) {
                this.router.navigate(['/error/403']);
            }
            if ([401].includes(err.status)) {
                this.loginService.logout();
            }
            const error = (err && err.error && err.error.error_description) || err.statusText;
            return throwError(error);
        }));
    }
}