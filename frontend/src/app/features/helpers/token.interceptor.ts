import { environment } from '../../../environments/environment';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {StorageService} from "../services/storage.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private storageService: StorageService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const accessToken = this.storageService.getItem(StorageService.ACCESS_TOKEN);
        const isLoggedIn = accessToken;
        const isApiUrl = req.url.startsWith(environment.apiUrl) && !req.url.endsWith('/oauth/token');

        if (isLoggedIn && isApiUrl) {
            req = req.clone({
                setHeaders: { Authorization: `Bearer ${accessToken}` }
            });
        }
        return next.handle(req);
    }
}
