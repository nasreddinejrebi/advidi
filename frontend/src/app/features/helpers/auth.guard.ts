import { StorageService } from './../services/storage.service';
import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    
    constructor(
        private router: Router,
        private storageSevice: StorageService
    ) { }

    canActivate() {
        const token = this.storageSevice.getItem(StorageService.ACCESS_TOKEN);
        if (token) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}

@Injectable({ providedIn: 'root' })
export class LoggedInGuard implements CanActivate {

    constructor(
        private router: Router,
        private storageSevice: StorageService
    ) { }

    canActivate() {
        const token = this.storageSevice.getItem(StorageService.ACCESS_TOKEN);
        if (token) {
            this.router.navigate(['/']);
            return false;
        } else {
            return true;
        }
    }
}