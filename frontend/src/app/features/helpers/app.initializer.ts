import { LoginService } from './../services/login.service';

export function appInitializer(loginservice: LoginService) {
    return () => new Promise(resolve => {
        // attempt to refresh token on app start up to auto authenticate
        loginservice.refreshToken()
            .subscribe()
            .add(resolve);
    });
}