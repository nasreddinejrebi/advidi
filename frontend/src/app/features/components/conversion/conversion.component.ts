import { MatPaginator } from '@angular/material/paginator';
import { OfferService } from './../../services/offer.service';
import { MatTableDataSource } from '@angular/material/table';
import { Offer } from './../../models/offer';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Conversion } from '../../models/conversion';

@Component({
  selector: 'app-conversion',
  templateUrl: './conversion.component.html',
  styleUrls: ['./conversion.component.scss']
})
export class ConversionComponent implements OnInit {

  offer: Offer;
  dataSource = new MatTableDataSource<any>();
  displayedColumns = ['affiliateId', 'payout', 'received', 'date'];
  conversions: Conversion[] = [];
  loading = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dateFrom = '';
  dateTo = '';

  constructor(@Inject(MAT_DIALOG_DATA) public dataDialog: any, private offerService: OfferService) {
    this.offer = dataDialog.offer
  }

  ngOnInit(): void {
    this.getConversions(0, 10)
  }

  dateFromChanged(dateFrom) {
    this.dateFrom = dateFrom;
    this.getConversions(this.paginator.pageIndex, this.paginator.pageSize);
  }

  dateToChanged(dateTo) {
    this.dateTo = dateTo;
    this.getConversions(this.paginator.pageIndex, this.paginator.pageSize);
  }

  getConversions(page, size) {
    this.offerService.getConversions(this.offer.id.toString(), page.toString(), size.toString(), this.dateFrom, this.dateTo)
      .subscribe((conversions: any) => {
        this.loading = false;
        this.conversions.length = page * size;
        this.conversions.push(...conversions.content);
        this.conversions.length = conversions.totalElements;
        this.dataSource = new MatTableDataSource<any>(this.conversions);
        this.dataSource._updateChangeSubscription();
        this.dataSource.paginator = this.paginator;
      })
  }

  pageChanged(event) {
    this.loading = true;
    const pageIndex = event.pageIndex;
    const pageSize = event.pageSize;
    this.getConversions(pageIndex, pageSize);
  }


}