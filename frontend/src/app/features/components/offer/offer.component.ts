import { ConversionComponent } from './../conversion/conversion.component';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { OfferService } from './../../services/offer.service';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Offer } from '../../models/offer';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource = new MatTableDataSource<any>();
  displayedColumns = ['id', 'name', 'action'];

  offers: any[] = [];
  filter: string = '';
  loading = true;


  constructor(private route: ActivatedRoute, private offerService: OfferService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.getOffers(0, 5)
  }

  goToLink(url) {
    window.open(url, '_blank');
  }

  showConversion(offer: Offer) {
    this.dialog.open(ConversionComponent, {
      data: {
        offer
      },
      panelClass: 'full-width-dialog'
    });
  }

  getOffers(page, size) {
    this.offerService.getOffers(page.toString(), size.toString(), this.filter)
      .subscribe((offers: any) => {
        this.loading = false;
        this.offers.length = page * size;
        this.offers = offers.content;
        this.offers.length = offers.totalElements;
        this.dataSource.data = this.offers;
        this.dataSource.paginator = this.paginator;
      })
  }

  filterOffers(filter) {
    this.filter = filter
    this.getOffers(0, 5)
  }


  getNextData(page, size) {

    this.offerService.getOffers(page.toString(), size.toString(), this.filter)
      .subscribe((offers: any) => {
        this.loading = false;
        this.offers.length = page * size;
        this.offers.push(...offers.content);
        this.offers.length = offers.totalElements;
        this.dataSource = new MatTableDataSource<any>(this.offers);
        this.dataSource._updateChangeSubscription();
        this.dataSource.paginator = this.paginator;

      })
  }
  pageChanged(event) {
    this.loading = true;

    const pageIndex = event.pageIndex;
    const pageSize = event.pageSize;
    this.getNextData(pageIndex, pageSize);
  }



}
