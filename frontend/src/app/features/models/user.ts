export class User {
    username: string;
    authorities: string[];
    token: string;
    refreshToken: string;
}