
export class Offer {
    id: number;
    url: string;
    name: string;
}