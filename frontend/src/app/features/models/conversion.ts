
export class Conversion {
    id: number;
    offerId: number;
    affiliateId: number;
    timestamp: Date;
    payout: number;
    received: number;
}