import { Conversion } from './../models/conversion';
import { Offer } from './../models/offer';
import { CustomPage } from './../models/custom-page';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";


@Injectable({ providedIn: 'root' })
export class OfferService {
    constructor(private http: HttpClient) { }

    getOffers(page: string, size: string, name: string) {
        return this.http.get<CustomPage<Offer>>(`${environment.apiUrl}/offers`, {
            params: {
                page,
                size,
                name
            }
        });
    }

    getConversions(offerId: string, page: string, size: string, from: string, to: string) {
        return this.http.get<CustomPage<Conversion>>(`${environment.apiUrl}/offers/${offerId}/conversions`, {
            params: {
                page,
                size,
                from,
                to
            }
        })
    }
}