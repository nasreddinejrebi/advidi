import { StorageService } from './storage.service';
import { Token } from './../models/token';
import { environment } from './../../../environments/environment';
import { User } from './../models/user';
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class LoginService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;
    private tokenSubject: BehaviorSubject<Token>;
    public token: Observable<Token>;

    constructor(private router: Router, private http: HttpClient, private storageService: StorageService) {
        this.userSubject = new BehaviorSubject<User>(null);
        this.user = this.userSubject.asObservable();
        this.tokenSubject = new BehaviorSubject<Token>(null);
        this.token = this.tokenSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }

    public get tokenValue(): Token {
        return this.tokenSubject.value;
    }

    login(username: string, password: string, cookies = document.cookie) {

        const headers = this.getHeaders();
        const body = `grant_type=password&username=${username}&password=${password}`;
        return this.http.post<any>(`${environment.apiUrl}/oauth/token`, body, headers)
            .pipe(map(token => {
                this.tokenSubject.next(token);
                this.storageService.setItem(StorageService.REFRESH_TOKEN, this.tokenValue.refresh_token);
                this.storageService.setItem(StorageService.ACCESS_TOKEN, this.tokenValue.access_token);
                this.startRefreshTokenTimer();
                return token;
            }));
    }

    getUserData() {
        this.http.get<User>(`${environment.apiUrl}/users/self`);
    }


    logout() {
        this.stopRefreshTokenTimer();
        this.storageService.clear();
        this.router.navigate(['/login']);
    }

    refreshToken() {
        const refreshToken = this.storageService.getItem(StorageService.REFRESH_TOKEN);
        const body = `grant_type=refresh_token&refresh_token=${refreshToken}`;
        const headers = this.getHeaders();
        return this.http.post<any>(`${environment.apiUrl}/oauth/token`, body, headers)
            .pipe(map(token => {
                this.tokenSubject.next(token);
                this.storageService.setItem(StorageService.REFRESH_TOKEN, this.tokenValue.refresh_token);
                this.storageService.setItem(StorageService.ACCESS_TOKEN, this.tokenValue.access_token);
                this.startRefreshTokenTimer();
                return token;
            }));
    }

    private getHeaders() {
       return  {
            headers: new HttpHeaders({
                'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
                Authorization: `Basic ${btoa('public:')}`
            })
        };
    }

    private refreshTokenTimeout;

    private startRefreshTokenTimer() {
        const token = JSON.parse(atob(this.tokenValue.access_token.split('.')[1]));
        const expires = new Date(token.exp * 1000);
        const timeout = (this.tokenValue.expires_in - 60) * 1000;
        console.log(timeout)
        this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout);
    }

    private stopRefreshTokenTimer() {
        clearTimeout(this.refreshTokenTimeout);
    }
}
