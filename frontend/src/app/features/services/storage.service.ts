import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class StorageService {
    public static readonly ACCESS_TOKEN: string = 'app_access_token';
    public static readonly REFRESH_TOKEN: string = 'app_refresh_token';

    private tokenSubject: BehaviorSubject<string>;
    private: BehaviorSubject<string>;

    constructor() {
        this.tokenSubject = new BehaviorSubject<string>(null);
    }

    get getTokenSubject() {
        return this.tokenSubject;
    }

    public getItem(identifier: string): string {
        const item = localStorage.getItem(identifier);
        if (identifier === StorageService.ACCESS_TOKEN) {
            this.tokenSubject.next(item);
        }
        return item;
    }


    public setItem(identifier: string, token: string): void {
        localStorage.setItem(identifier, token);
        if (identifier === localStorage.ACCESS_TOKEN) {
            this.tokenSubject.next(token);
        }
    }

    public clear() {
        localStorage.clear();
        this.tokenSubject.next(null);
    }
}