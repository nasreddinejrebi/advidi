import { ErrorComponent } from './features/components/error/error.component';
import { AuthGuard, LoggedInGuard } from './features/helpers/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './features/components/login/login.component';
import { HomeComponent } from './features/components/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent , canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent , canActivate: [LoggedInGuard]},
  { path: 'error/:errorCode', component: ErrorComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '/error/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
