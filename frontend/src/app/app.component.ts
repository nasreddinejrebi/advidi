import { StorageService } from './features/services/storage.service';
import { LoginService } from './features/services/login.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  token: string;

  constructor(private loginService: LoginService, private storageService: StorageService) {
    storageService.getTokenSubject.subscribe(token => this.token = token);
  }

  logout() {
    this.loginService.logout();
  }
}
