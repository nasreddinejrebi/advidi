# Advidi Console


## Prerequisite
You need to install this tools: 

- `java` version 8 (tested with 1.8.0_212)
- `maven` version 3 (tested with 3.6.1)
- `node.js` version 10 (tested with 10.16.0)
- `npm` version 6 (tested with 6.9.0)
- `docker` version 19 (tested with 19.03.5)
- `docker-compose` version 1 (tested with 1.24.1)

## Build the application components

- **Backend component**

In order to run our backend we need to import dependencies and generate our application artifact by running the following commands: 

* `cd backend`
* `mvn -B -DskipTests package`
* `mvn -B test (Optional just to run tests)`  
-------------------------------------------------
- **Frontend component**

In order to run our frontend we need to import dependencies and generate our application artifact by executing the following commands:

* `cd ..` (return to root folder)
* `cd frontend`
* `npm install`
* `npm run build`

## Run the application

You don't need to install any server in your computer, all application components are containerized (database, backend, frontend) you only need  to have docker and docker compose installed. 

To run your application you need to have These ports available (4200, 3306, 8080) and then execute the following commands:

* `cd ..` (return to root folder)
* `docker-compose up --build`

## Access to different servers of the application

NOTE: We have two users in the application: 

admin with role admin:

    - Username: admin 
    - Password: admin 
    - Role: ADMIN
    - this User Can access to all data of our application  

User with role user:

    - Username: user 
    - Password: user 
    - Role: USER
    - this User doesn't have right to access to offers and conversions so we can test security access in the application using this user  
    
 - Frontend
    * You can access to the frontend application using this URL: http://localhost:4200/
 - Backend
    * You can import postman collection and test different apis using advidi.postman_collection.json file in the root of the project or using this url https://www.getpostman.com/collections/c6c7acf01d9c6ae587de
 - Mysql
    * You can access to our databases using mysql workbench with these credentials:   
        - HOST: localhost
        - PORT: 3306
        - Username: root 
        
        - Password: root 

