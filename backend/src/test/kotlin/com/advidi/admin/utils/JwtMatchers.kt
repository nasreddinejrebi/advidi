package com.advidi.admin.utils

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.springframework.security.jwt.JwtHelper

fun isJwtToken(vararg matchers: Matcher<JsonNode>): Matcher<String> {
    return object : BaseMatcher<String>() {

        override fun describeTo(description: Description) {
            description.appendText("JWT token with: ")
            matchers.forEach {
                description.appendText("\n - ")
                description.appendDescriptionOf(it)
            }
        }

        override fun matches(item: Any): Boolean {
            if (item is String) {
                val jwt = JwtHelper.decode(item)
                val token = ObjectMapper().readTree(jwt.claims)
                return matchers
                        .map { it.matches(token) }
                        .reduce { a, b -> a && b }
            }
            return false
        }
    }
}

fun withStringAttribute(name: String, matcher: Matcher<String>): Matcher<JsonNode> {
    return object : BaseMatcher<JsonNode>() {

        override fun describeTo(description: Description) {
            description.appendText("attribute $name ")
            matcher.describeTo(description)
        }

        override fun matches(item: Any): Boolean {
            if (item is JsonNode) {
                return matcher.matches(item.get(name).textValue())
            }
            return false
        }
    }
}

fun withArrayAttribute(name: String, matcher: Matcher<Iterable<String>>): Matcher<JsonNode> {
    return object : BaseMatcher<JsonNode>() {

        override fun describeTo(description: Description) {
            description.appendText("attribute $name")
            matcher.describeTo(description)
        }

        override fun matches(item: Any): Boolean {
            if (item is JsonNode && item.has(name)) {
                return matcher.matches(
                        item.get(name)
                                .map { it.textValue() }
                )
            }
            return false
        }
    }
}