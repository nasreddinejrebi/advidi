package com.advidi.admin.utils

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

@Component
class ClientAuthentication(private val mockMvc: MockMvc) {

    fun login(username: String, password: String): OAuth2Response {
        val responseBody = mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "password")
                        .param("username", username)
                        .param("password", password)
        )
                .andReturn().response.contentAsString
        return ObjectMapper().readValue(responseBody, OAuth2Response::class.java)
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class OAuth2Response(
        @JsonProperty("access_token")
        private val accessToken: String,
        @JsonProperty("refresh_token")
        private val refreshToken: String
) {
    fun getAuthorizationHeader() = "Bearer $accessToken"
}