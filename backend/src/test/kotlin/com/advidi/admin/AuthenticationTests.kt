package com.advidi.admin

import com.advidi.admin.config.IntegrationTest
import com.advidi.admin.utils.ClientAuthentication
import com.advidi.admin.utils.isJwtToken
import com.advidi.admin.utils.withArrayAttribute
import com.advidi.admin.utils.withStringAttribute
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner::class)
@IntegrationTest
class AuthenticationTests {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var clientAuth: ClientAuthentication

    @Test
    fun `can login using oauth and right username and password`() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "password")
                        .param("username", "admin")
                        .param("password", "admin")
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.token_type").value(CoreMatchers.equalTo("bearer")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty)
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").value(
                        isJwtToken(
                                withStringAttribute("user_name", CoreMatchers.equalTo("admin")),
                                withArrayAttribute("authorities", CoreMatchers.hasItems(
                                        "ADMIN"
                                ))
                        )
                ))
    }

    @Test
    fun `can't login using wrong username`() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "password")
                        .param("username", "invalid_username")
                        .param("password", "pass")
        )
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").value("invalid_grant"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error_description").value("Bad credentials"))
    }

    @Test
    fun `can't login using wrong password`() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "password")
                        .param("username", "admin")
                        .param("password", "invalid_pass")
        )
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").value("invalid_grant"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error_description").value("Bad credentials"))
    }

    @Test
    fun `refresh token can be used to obtain a new token`() {
        val responseBody = mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "password")
                        .param("username", "admin")
                        .param("password", "admin")
        )
                .andReturn().response.contentAsString

        val accessTokenString = ObjectMapper().readTree(responseBody)
        val accessToken = accessTokenString.get("access_token").textValue()
        val refreshToken = accessTokenString.get("refresh_token").textValue()

        mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "refresh_token")
                        .param("refresh_token", refreshToken)
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.token_type").value(CoreMatchers.equalTo("bearer")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty)
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").value(CoreMatchers.not(CoreMatchers.equalTo(accessToken))))
    }


    @Test
    fun `refresh token can be used more than once`() {
        val responseBody = mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "password")
                        .param("username", "admin")
                        .param("password", "admin")
        )
                .andReturn().response.contentAsString

        val accessTokenString = ObjectMapper().readTree(responseBody)
        val accessToken = accessTokenString.get("access_token").textValue()
        val refreshToken = accessTokenString.get("refresh_token").textValue()

        mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "refresh_token")
                        .param("refresh_token", refreshToken)
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.token_type").value(CoreMatchers.equalTo("bearer")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty)
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").value(CoreMatchers.not(CoreMatchers.equalTo(accessToken))))

        mockMvc.perform(
                MockMvcRequestBuilders.post("/oauth/token")
                        .with(SecurityMockMvcRequestPostProcessors.httpBasic("public", ""))
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("grant_type", "refresh_token")
                        .param("refresh_token", refreshToken)
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.token_type").value(CoreMatchers.equalTo("bearer")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty)
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").value(CoreMatchers.not(CoreMatchers.equalTo(accessToken))))
    }


    @Test
    fun `can view my own user profile`() {
        val auth = clientAuth.login("admin", "admin")
        mockMvc.perform(
                MockMvcRequestBuilders.get("/users/self")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(Matchers.equalTo("admin")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.active").value(Matchers.equalTo(true)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.role.name").value(Matchers.equalTo("ADMIN")))
    }

}
