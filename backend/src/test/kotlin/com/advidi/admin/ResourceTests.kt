package com.advidi.admin

import com.advidi.admin.config.IntegrationTest
import com.advidi.admin.dao.offer.Offer
import com.advidi.admin.utils.ClientAuthentication
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner::class)
@IntegrationTest
class ResourceTests {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var clientAuth: ClientAuthentication

    @Test
    fun `can login with admin user and get offer list`() {
        val auth = clientAuth.login("admin", "admin")
        mockMvc.perform(
                MockMvcRequestBuilders.get("/offers")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(Matchers.hasSize<Any>(Matchers.greaterThanOrEqualTo(0))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.pageable.pageSize").value(Matchers.equalTo(20)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(Matchers.equalTo(true)))
    }

    @Test
    fun `can login with admin user and get offer filtred`() {
        val auth = clientAuth.login("admin", "admin")
        val offers = getOfferList(auth.getAuthorizationHeader())
        mockMvc.perform(
                MockMvcRequestBuilders.get("/offers?name=${offers[0].name}")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(Matchers.hasSize<Any>(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].name").value(Matchers.equalTo(offers[0].name)))
    }

    @Test
    fun `can login with admin user and get paginated offer with specific page and size`() {
        val auth = clientAuth.login("admin", "admin")
        mockMvc.perform(
                MockMvcRequestBuilders.get("/offers?page=1&size=10")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(Matchers.hasSize<Any>(Matchers.greaterThanOrEqualTo(0))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(Matchers.hasSize<Any>(Matchers.lessThanOrEqualTo(10))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.pageable.pageSize").value(Matchers.equalTo(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.pageable.pageNumber").value(Matchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(Matchers.equalTo(false)))
    }

    @Test
    fun `can login with simple user and get forbidden access to get offers`() {
        val auth = clientAuth.login("user", "user")
        mockMvc.perform(
                MockMvcRequestBuilders.get("/offers")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @Test
    fun `can login with admin user and get the conversion list of one offer`() {
        val auth = clientAuth.login("admin", "admin")
        val offers = getOfferList(auth.getAuthorizationHeader())
        mockMvc.perform(
                MockMvcRequestBuilders.get("/offers/${offers[0].id}/conversions")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(Matchers.hasSize<Any>(Matchers.greaterThanOrEqualTo(0))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.pageable.pageSize").value(Matchers.equalTo(20)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(Matchers.equalTo(true)))
    }

    @Test
    fun `can login with simple user and get forbidden access to get conversions`() {
        val auth = clientAuth.login("user", "user")
        mockMvc.perform(
                MockMvcRequestBuilders.get("/offers/1/conversions")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isForbidden)
    }

    @Test
    fun `can login with admin user and get 404 when offer not found`() {
        val auth = clientAuth.login("admin", "admin")
        mockMvc.perform(
                MockMvcRequestBuilders.get("/offers/0/conversions")
                        .header(HttpHeaders.AUTHORIZATION, auth.getAuthorizationHeader())
        )
                .andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    fun getOfferList(token: String): List<Offer> {
        val responseBody = mockMvc.perform(
                MockMvcRequestBuilders.get("/offers")
                        .header(HttpHeaders.AUTHORIZATION, token)
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(Matchers.hasSize<Any>(Matchers.greaterThanOrEqualTo(1))))
                .andReturn().response.contentAsString

        val offers = jacksonObjectMapper().readValue<CustomPageImpl<Offer>>(responseBody)
        return offers.content!!
    }
}