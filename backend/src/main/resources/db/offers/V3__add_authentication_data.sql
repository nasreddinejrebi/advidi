CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL
);

ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `ro_id` bigint(20) NOT NULL
);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_role` FOREIGN KEY (`ro_id`) REFERENCES `role` (`id`);


INSERT INTO `role` (`id`, `name`) VALUES
    (1, 'ADMIN');

INSERT INTO `role` (`id`, `name`) VALUES
    (2, 'USER');

INSERT INTO `user` (`id`, `active`, `password`, `username`, `ro_id`) VALUES
    (1, b'1', '$2a$10$kis/0l0N9hpkZXaqfIP0Eu/lXN...OwdqF3ethv5DMucXTxyYqZny', 'admin', 1),
    (2, b'1', '$2a$10$OhpdJyJqWZY40jmzZGuf2unh1LEYkGErjEA73DDXdoBNwAeUUo1lC', 'user', 2);