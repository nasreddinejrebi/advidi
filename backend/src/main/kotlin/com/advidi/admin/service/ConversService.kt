package com.advidi.admin.service

import com.advidi.admin.dao.conversion.Conversion
import com.advidi.admin.dao.conversion.ConversionRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*
import java.util.Calendar


@Service
class ConversService(var conversionRepository: ConversionRepository, var offerRepository: ConversionRepository) {


    fun getConversionList(pageable: Pageable, offerId: Long, from: Date?, to: Date?): Page<Conversion> {
        offerRepository.findById(offerId).orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }

        return if (from != null && to != null) {
            conversionRepository.findAllByOfferIdAndTimestampBetween(pageable, offerId, getDateEndDay(from), getDateEndDay(to))
        } else if (from != null) {
            conversionRepository.findAllByOfferIdAndTimestampGreaterThanEqual(pageable, offerId, getDateEndDay(from))
        } else if (to != null) {
            conversionRepository.findAllByOfferIdAndTimestampLessThanEqual(pageable, offerId, getDateEndDay(to))
        } else {
            conversionRepository.findAllByOfferId(pageable, offerId)
        }
    }

    private fun getDateEndDay(date: Date): Date {
        val cl = Calendar.getInstance()
        cl.time = date
        cl.set(Calendar.HOUR_OF_DAY, 23)
        cl.set(Calendar.MINUTE, 59)
        cl.set(Calendar.SECOND, 59)
        cl.set(Calendar.MILLISECOND, 999)
        return cl.time
    }
}