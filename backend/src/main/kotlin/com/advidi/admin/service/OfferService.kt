package com.advidi.admin.service

import com.advidi.admin.dao.offer.Offer
import com.advidi.admin.dao.offer.OfferRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class OfferService(private val offerRepository: OfferRepository) {
    fun getOfferList(pageable: Pageable, name: String): Page<Offer> {
        return offerRepository.findAllByNameContains(pageable, name)
    }
}