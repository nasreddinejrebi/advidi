package com.advidi.admin.service

import com.advidi.admin.dao.user.User
import com.advidi.admin.dao.user.UserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.RuntimeException


@Service
@Transactional
class UserService(val userRepository: UserRepository) {


    @Transactional(readOnly = true)
    fun findByUsername(username: String): User {
        return userRepository.findByUsername(username) ?: throw UsernameNotFoundException()
    }

}

class UsernameNotFoundException :  RuntimeException()