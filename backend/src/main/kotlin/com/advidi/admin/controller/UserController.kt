package com.advidi.admin.controller

import com.advidi.admin.dao.user.User
import com.advidi.admin.service.UserService
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/users")
class UserController(private val userService: UserService) {

    @GetMapping("/self")
    fun self(user: Principal): User = userService.findByUsername(user.name)
}
