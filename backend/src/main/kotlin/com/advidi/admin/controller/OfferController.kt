package com.advidi.admin.controller

import com.advidi.admin.dao.conversion.Conversion
import com.advidi.admin.dao.offer.Offer
import com.advidi.admin.service.ConversService
import com.advidi.admin.service.OfferService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.SortDefault
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/offers")
@PreAuthorize("hasAnyAuthority('ADMIN')")
class OfferController(private val offerService: OfferService,
                      private val conversService: ConversService) {

    @GetMapping
    fun getOfferList(@SortDefault(sort = ["name"], direction = Sort.Direction.ASC) pageable: Pageable, @RequestParam(defaultValue = "") name: String): Page<Offer> {
        return offerService.getOfferList(pageable, name)
    }

    @GetMapping("/{offerId}/conversions")
    fun getConversionList(@SortDefault(sort = ["timestamp"], direction = Sort.Direction.DESC) pageable: Pageable, @PathVariable offerId: Long, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") from: Date?, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") to: Date?): Page<Conversion> {
        return conversService.getConversionList(pageable, offerId, from, to)
    }
}