package com.advidi.admin.config

import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct
import javax.sql.DataSource

@Configuration
class FlywayInitializer {
    @Autowired
    private val offerDataSource: DataSource? = null
    @Autowired
    private val conversionDataSource: DataSource? = null

    @PostConstruct
    fun migrateFlyway() {

        val conversionFlyway = Flyway.configure().table("conversions_flyway").dataSource(conversionDataSource).baselineOnMigrate(true).locations( "db/conversions").load()
        conversionFlyway.migrate()

        val offerFlyway = Flyway.configure().table("offer_flyway").dataSource(offerDataSource).baselineOnMigrate(true).locations( "db/offers").load()
        offerFlyway.migrate()
    }
}