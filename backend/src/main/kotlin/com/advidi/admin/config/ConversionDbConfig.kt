package com.advidi.admin.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import java.util.*
import javax.sql.DataSource


@Configuration
@EnableJpaRepositories(
        basePackages = ["com.advidi.admin.dao.conversion"],
        entityManagerFactoryRef = "conversionEntityManager",
        transactionManagerRef = "conversionTransactionManager"
)
class ConversionDbConfig {

    @Value("\${spring.jpa.hibernate.ddl-auto}")
    lateinit var ddlAuto: String


    @Bean
    @ConfigurationProperties(prefix = "conversion.datasource")
    fun conversionDataSource(): DataSource = DataSourceBuilder.create().build()

    @Bean
    fun conversionEntityManager(): LocalContainerEntityManagerFactoryBean {
        val jpaProperties = Properties()
        jpaProperties["hibernate.hbm2ddl.auto"] = ddlAuto

        val factory = LocalContainerEntityManagerFactoryBean()
        factory.dataSource = conversionDataSource()
        factory.jpaVendorAdapter = HibernateJpaVendorAdapter()
        factory.setJpaProperties(jpaProperties)
        factory.setPackagesToScan("com.advidi.admin.dao.conversion")
        return factory
    }


    @Bean
    fun conversionTransactionManager() = JpaTransactionManager(conversionEntityManager().`object`!!)
}