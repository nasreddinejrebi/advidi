package com.advidi.admin.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import javax.sql.DataSource
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Primary
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import java.util.*

@Configuration
@EnableJpaRepositories(
        basePackages = ["com.advidi.admin.dao.offer", "com.advidi.admin.dao.user"],
        entityManagerFactoryRef = "offerEntityManager",
        transactionManagerRef= "offerTransactionManager")
class OfferDbConfig {

    @Value("\${spring.jpa.hibernate.ddl-auto}")
    lateinit var  ddlAuto: String

    @Bean
    @ConfigurationProperties(prefix = "offer.datasource")
    fun offerDataSource(): DataSource = DataSourceBuilder.create().build()

    @Bean
    fun offerEntityManager(): LocalContainerEntityManagerFactoryBean {
        val jpaProperties = Properties()
        jpaProperties["hibernate.hbm2ddl.auto"] = ddlAuto

        val factory = LocalContainerEntityManagerFactoryBean()
        factory.dataSource = offerDataSource()
        factory.jpaVendorAdapter = HibernateJpaVendorAdapter()
        factory.setJpaProperties(jpaProperties)
        factory.setPackagesToScan("com.advidi.admin.dao.offer", "com.advidi.admin.dao.user")
        return factory
    }

    @Bean
    @Primary
    fun offerTransactionManager() = JpaTransactionManager(offerEntityManager().`object`!!)
}
