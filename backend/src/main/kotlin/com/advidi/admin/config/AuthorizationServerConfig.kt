package com.advidi.admin.config

import com.advidi.admin.security.CustomAccessTokenConverter
import com.advidi.admin.security.CustomUserDetailsService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.token.DefaultTokenServices
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import java.security.KeyPairGenerator
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter


@Configuration
class AuthorizationServerConfig(private val authenticationManager: AuthenticationManager,
                                private val userDetailsService: CustomUserDetailsService,
                                private val accessTokenConverter: CustomAccessTokenConverter,
                                private val passwordEncoder: PasswordEncoder) : AuthorizationServerConfigurerAdapter() {

    override fun configure(oauthServer: AuthorizationServerSecurityConfigurer) {
        oauthServer
                .passwordEncoder(passwordEncoder)
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()")
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {

        endpoints
                .tokenStore(tokenStore())
                .tokenEnhancer(accessTokenConverter())
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService)
    }

    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.inMemory()
                .withClient("public")
                .authorizedGrantTypes("refresh_token", "password")
                .scopes("*")
                .accessTokenValiditySeconds(600)
                .refreshTokenValiditySeconds(43200)
    }

    @Bean
    fun tokenStore(): TokenStore = JwtTokenStore(accessTokenConverter())

    @Bean
    fun accessTokenConverter(): JwtAccessTokenConverter {
        val keyPair = KeyPairGenerator.getInstance("RSA")
                .genKeyPair()
        val converter = JwtAccessTokenConverter()
        converter.setKeyPair(keyPair)
        converter.accessTokenConverter = accessTokenConverter
        return converter
    }

    @Bean
    @Primary
    fun tokenServices(): DefaultTokenServices {
        val defaultTokenServices = DefaultTokenServices()
        defaultTokenServices.setTokenStore(tokenStore())
        defaultTokenServices.setSupportRefreshToken(true)
        return defaultTokenServices
    }

}