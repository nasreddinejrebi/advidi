package com.advidi.admin.security

import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter
import org.springframework.stereotype.Component

@Component
class CustomAccessTokenConverter(private val userDetailsService: CustomUserDetailsService) : DefaultAccessTokenConverter() {

    override fun extractAuthentication(claims: MutableMap<String, *>): OAuth2Authentication {
        val authentication = super.extractAuthentication(claims)
        authentication.details = userDetailsService.loadUserByUsername(claims["user_name"] as String)
        return authentication
    }
}