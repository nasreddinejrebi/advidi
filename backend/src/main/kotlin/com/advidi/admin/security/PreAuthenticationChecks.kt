package com.advidi.admin.security

import org.springframework.security.authentication.DisabledException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsChecker

class PreAuthenticationChecks : UserDetailsChecker {

    override fun check(user: UserDetails) {
        user.isEnabled || throw DisabledException("account_disabled")
    }
}