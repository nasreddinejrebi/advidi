package com.advidi.admin.dao.user

import javax.persistence.*

@Entity
@Table(name="role")
data class Role(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        val name: String = ""
)