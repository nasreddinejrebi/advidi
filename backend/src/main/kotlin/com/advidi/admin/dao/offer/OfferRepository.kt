package com.advidi.admin.dao.offer

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OfferRepository : JpaRepository<Offer, Long> {
    fun findAllByNameContains(pageable: Pageable, name: String) : Page<Offer>
}