package com.advidi.admin.dao.offer

import javax.persistence.*

@Entity
@Table(name="offer")
data class Offer (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        val url: String = "",

        val name: String = ""
)

