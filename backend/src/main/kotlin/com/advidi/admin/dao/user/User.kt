package com.advidi.admin.dao.user

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*


@Entity
@Table(name="user")
data class User(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        val username: String,

        @JsonIgnore
        val password: String,

        val active: Boolean,

        @ManyToOne(optional=false)
        @JoinColumn(name = "ro_id", updatable = false, insertable = false)
        val role: Role

) {
    constructor() : this(
            id = 0,
            username = "",
            password = "",
            active = true,
            role = Role(0, "")
    )
}