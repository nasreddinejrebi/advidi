package com.advidi.admin.dao.conversion

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ConversionRepository : JpaRepository<Conversion, Long> {
    fun findAllByOfferIdAndTimestampBetween(pageable: Pageable, offerId: Long, from: Date, to: Date): Page<Conversion>
    fun findAllByOfferIdAndTimestampGreaterThanEqual(pageable: Pageable, offerId: Long, from: Date): Page<Conversion>
    fun findAllByOfferIdAndTimestampLessThanEqual(pageable: Pageable, offerId: Long, to: Date): Page<Conversion>
    fun findAllByOfferId(pageable: Pageable, offerId: Long): Page<Conversion>
}