package com.advidi.admin.dao.conversion

import java.util.*
import javax.persistence.*

@Entity
@Table(name="conversion")
data class Conversion(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        @Column(name="offer_id")
        val offerId: Long = 0,

        @Column(name="affiliate_id")
        val affiliateId: Long = 0,

        @Basic(optional = false)
        @Column(name = "timestamp", insertable = false, updatable = false)
        @Temporal(TemporalType.TIMESTAMP)
        val timestamp: Date = Date(),

        val payout: Double = 0.0,

        val received: Double = 0.0
)
